function generar(){
    let limite = document.getElementById('limite').value; 
    let lstNumeros = document.getElementById('idNumeros');
    //var num = random(); 
    var pares=0, totalP=0, totalI=0, impares=0;
    
    const arreglo = [];
    for(let i=0; i<limite; i++){
        arreglo.push(Math.floor(Math.random() * (50 - 1) + 1));
    }
    for(let i=0; i<limite; i++){
        //num = random();
        //lstNumeros.options[i] = new Option(num, 'texto', i+1);
        arreglo.sort((a,b)=>b-a);
        lstNumeros.options[i] = new Option(arreglo[i], 'texto', i+1);

        //Contar pares e impares
        if(arreglo[i] % 2 == 0){
            pares = pares+1;
            console.log("par:"+arreglo[i]);
        }else{
            impares = impares+1;
            console.log("impar:"+arreglo[i]);
        }
    }
    console.log("pares:"+pares);
    console.log("impares:"+impares);

    //Sacar porcentajes
    totalP = (pares*100)/limite;
    totalI = (impares*100)/limite;

    let cantPares = document.getElementById('pares'); cantPares.innerHTML = cantPares.innerHTML + totalP.toFixed(2) + "%"; 
    let cantImpar = document.getElementById('impares'); cantImpar.innerHTML = cantImpar.innerHTML + totalI.toFixed(2) + "%";
    
    let simetrico = document.getElementById('simetrico');
    if(totalP>=totalI){
        if((totalP-totalI)>25){
            simetrico.innerHTML = simetrico.innerHTML + "No es simétrico";
        }else{
            simetrico.innerHTML = simetrico.innerHTML + "Sí es simétrico";
        }
    }
    else if(totalI>=totalP){
        if((totalI-totalP)>25){
            simetrico.innerHTML = simetrico.innerHTML + "No es simétrico";
        }else{
            simetrico.innerHTML = simetrico.innerHTML + "Sí es simétrico";
        }
    }
}

function calcular(){
    //crear arreglo random 
    const arreglo = [];
    for(let i=0; i<20; i++){
        arreglo.push(Math.floor(Math.random() * (50 - 1) + 1));
    }
    //imprimir arreglo
    let listaNum = document.getElementById('listaNum');
    listaNum.innerHTML = listaNum.innerHTML + arreglo + "<br/>";
    console.log(arreglo);

    //1. Función que calcula el promedio del arreglo
    function promedio(arreglo){
        var sum=0, prom=0;
        for(let i=0; i<arreglo.length; i++){
            sum = sum + arreglo[i];
            //prom = (prom + arreglo[i])/arreglo.length;
        }
        prom = sum/arreglo.length;
        //imprimir promedio
        let mostrarProm = document.getElementById('promedio');
        mostrarProm.innerHTML = mostrarProm.innerHTML + prom + "<br/>";
        console.log("promedio: " + prom);
        return prom;
    }

    //2. Función que cuenta el numero de pares de un arreglo
    function pares(arreglo){
        var pares=0;
        for(let i=0; i<arreglo.length; i++){
            if(arreglo[i] % 2 == 0){
                pares = pares + 1;
            }
        }
        //imprimir pares
        let mostrarPares = document.getElementById('contadorPares');
        mostrarPares.innerHTML = mostrarPares.innerHTML + pares + "<br/>"; 
    }

    //3. Función que ordena el arreglo de mayor a menor 
    function ordenar(arreglo){
        let mostrarArrayOrdenado = document.getElementById('ordenar');
        mostrarArrayOrdenado.innerHTML = mostrarArrayOrdenado.innerHTML + arreglo.sort(function(a,b){return b-a}) + "<br/>";
        //arreglo.sort(function(a,b){return b-a});
    }

    //llamar las funciones para que las imprimir
    promedio(arreglo);
    pares(arreglo);
    ordenar(arreglo);
}
